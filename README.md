## LAB 4


****************************************************************
    Lab1 : Terraform Hashicorp Learn
****************************************************************
​
https://learn.hashicorp.com/terraform
​
****************************************************************
    Lab2 : Les Bases de Terraform
****************************************************************
1-Create AWS Profile
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html
$ mkdir /home/stagiaire/.aws
$ gedit credentials
$ cat credentials
[walidsaad]
aws_access_key_id=AKaa
aws_secret_access_key=Draaa
​
$ export AWS_PROFILE=walidsaad
​
2-Install Terraform & VSCode
https://www.terraform.io/downloads.html
​
3-Create lab directory
$ mkdir lab2
4-Create TF template for AWS Provider
$ cd lab2
$ cat provider.tf
terraform {
  
  required_providers{
      aws= {
          source = "hashicorp/aws"
          version = "~> 3.27"
      }
  }
}
provider "aws" {
    profile = "default"
    region ="us-east-1"
  
}
​
5-Download AWS Plugin
​
$ terraform init
​
6-Create EC2 Instance
$ cat ec2-instance.tf
resource "aws_instance" "example" {
  ami           = "ami-0a54aef4ef3b5f881"
  instance_type = "t2.micro"
​
  provisioner "local-exec" {
    command = "echo ${aws_instance.example.public_ip} > ip_address.txt"
  }
}
​
7-Execution plan
​
$terraform plan
​
8-Deploy Infrastructure
​
$ terraform apply
$ cat ip_address.txt
​
9-Destroy Infrastructure
​
$ terraform destroy
​
10-Update TF template (Create Key Pair, EC2 Instance and Remote Exec)
​
$ cat ec2-instance.tf
​
resource "aws_key_pair" "example" {
  key_name   = "examplekey"
  public_key = file("~/.ssh/id_rsa.pub")
}
​
resource "aws_instance" "example" {
  key_name      = aws_key_pair.example.key_name
  ami           = "ami-0915bcb5fa77e4892"
  instance_type = "t2.micro"
​
  provisioner "local-exec" {
    command = "echo ${aws_instance.example.public_ip} > ip_address.txt"
  }
​
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("~/.ssh/id_rsa")
    host        = self.public_ip
  }
​
  provisioner "remote-exec" {
    inline = [
      "sudo yum -y update",
      "sudo yum -y install httpd",
      "sudo systemctl enable httpd",
      "sudo systemctl start httpd"
    ]
  }
}
$ ssh-keygen -t rsa
$ chmod 400 ~/.ssh/
$ terraform plan
$ terraform apply
​
****************************************************************
    Lab3 : AWS Use Case: Amazon S3
****************************************************************
1-Create lab directory
​
$ mkdir lab3
​
2-Create TF template for AWS Provider
​
$ cd lab3
$ cat provider.tf
terraform {
  
  required_providers{
      aws= {
          source = "hashicorp/aws"
          version = "~> 3.27"
      }
  }
}
provider "aws" {
    profile = "default"
    region ="us-east-1"
  
}
​
3-Download AWS Plugin
​
$ terraform init
​
4-Create Bucket S3
$ cat s3-bucket.tf
resource "aws_s3_bucket" "my-bucket-example"{
   bucket = "my-bucket-example-09072000"
   acl = "public-read"
}
resource "aws_s3_bucket_object" "picture-of-cat" {
    bucket =  aws_s3_bucket.my-bucket-example.id
    key = "cat.jpg"
    acl = "public-read"
    source = "./cat.jpg"
}
​
$ terraform plan
$ terraform apply
$ terraform destroy
****************************************************************
    Lab4 : AWS Use Case: Les Opérations Avancées de Terraform (VPC, EC2, RDS)
****************************************************************
​
1-Create provider.tf
​
terraform {
  
  required_providers{
      aws= {
          source = "hashicorp/aws"
          version = "~> 3.27"
      }
  }
}
​
provider "aws" {
    profile = "default"
    region ="us-east-1"
  
}
​
2-Create variables.tf
​
variable "AWS_REGION" {
default = "us-east-1"
}
​
variable "AMIS" {
    type = map
    default = {
        us-east-1 = "ami-0915bcb5fa77e4892"
        us-east-2 = "ami-05692172625678b4e"
        us-west-2 = "ami-0352d5a37fb4f603f"
        us-west-1 = "ami-0f40c8f97004632f9"
    }
}
​
variable "PATH_TO_PRIVATE_KEY" {
  default = "~/.ssh/id_rsa"
}
​
variable "PATH_TO_PUBLIC_KEY" {
  default = "~/.ssh/id_rsa.pub"
}
​
3-Create vpc.tf
​
#Create AWS VPC
resource "aws_vpc" "levelupvpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
​
  tags = {
    Name = "levelupvpc"
  }
}
​
# Public Subnets in Custom VPC
resource "aws_subnet" "levelupvpc-public-1" {
  vpc_id                  = aws_vpc.levelupvpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1a"
​
  tags = {
    Name = "levelupvpc-public-1"
  }
}
​
# Private Subnets in Custom VPC
resource "aws_subnet" "levelupvpc-private-1" {
  vpc_id                  = aws_vpc.levelupvpc.id
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-1a"
​
  tags = {
    Name = "levelupvpc-private-1"
  }
}
​
resource "aws_subnet" "levelupvpc-private-2" {
  vpc_id                  = aws_vpc.levelupvpc.id
  cidr_block              = "10.0.5.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-1b"
​
  tags = {
    Name = "levelupvpc-private-2"
  }
}
​
​
# Custom internet Gateway
resource "aws_internet_gateway" "levelup-gw" {
  vpc_id = aws_vpc.levelupvpc.id
​
  tags = {
    Name = "levelup-gw"
  }
}
​
#Routing Table for the Custom VPC
resource "aws_route_table" "levelup-public" {
  vpc_id = aws_vpc.levelupvpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.levelup-gw.id
  }
​
  tags = {
    Name = "levelup-public-1"
  }
}
​
resource "aws_route_table_association" "levelup-public-1-a" {
  subnet_id      = aws_subnet.levelupvpc-public-1.id
  route_table_id = aws_route_table.levelup-public.id
}
​
4- Create security_group.tf
​
#Security Group for levelupvpc
resource "aws_security_group" "allow-levelup-ssh" {
  vpc_id      = aws_vpc.levelupvpc.id
  name        = "allow-levelup-ssh"
  description = "security group that allows ssh connection"
​
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
​
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  tags = {
    Name = "allow-levelup-ssh"
  }
}
​
#Security Group for MariaDB
resource "aws_security_group" "allow-mariadb" {
  vpc_id      = aws_vpc.levelupvpc.id
  name        = "allow-mariadb"
  description = "security group for Maria DB"
​
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
​
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [aws_security_group.allow-levelup-ssh.id]
  }
  
  tags = {
    Name = "allow-mariadb"
  }
}
​
​
5- Create create_instance.tf
​
resource "aws_key_pair" "levelup_key" {
    key_name = "levelup_key"
    public_key = file(var.PATH_TO_PUBLIC_KEY)
}
​
#Create AWS Instance
resource "aws_instance" "MyFirstInstnace" {
  ami           = lookup(var.AMIS, var.AWS_REGION)
  instance_type = "t2.micro"
  //availability_zone = "us-east-2a"
  key_name      = aws_key_pair.levelup_key.key_name
  vpc_security_group_ids = [aws_security_group.allow-levelup-ssh.id]
  subnet_id = aws_subnet.levelupvpc-public-1.id
​
  tags = {
    Name = "custom_instance"
  }
}
​
output "public_ip" {
  value = aws_instance.MyFirstInstnace.public_ip 
}
​
​
6- Create rds.tf
​
#RDS Resources
resource "aws_db_subnet_group" "mariadb-subnets" {
  name        = "mariadb-subnets"
  description = "Amazon RDS subnet group"
  subnet_ids  = [aws_subnet.levelupvpc-private-1.id, aws_subnet.levelupvpc-private-2.id]
}
​
#RDS Parameters
resource "aws_db_parameter_group" "levelup-mariadb-parameters" {
  name        = "levelup-mariadb-parameters"
  family      = "mariadb10.4"
  description = "MariaDB parameter group"
​
  parameter {
    name  = "max_allowed_packet"
    value = "16777216"
  }
}
​
#RDS Instance properties
resource "aws_db_instance" "levelup-mariadb" {
  allocated_storage       = 20             # 20 GB of storage
  engine                  = "mariadb"
  engine_version          = "10.4.8"
  instance_class          = "db.t2.micro"  # use micro if you want to use the free tier
  identifier              = "mariadb"
  name                    = "mariadb"
  username                = "root"           # username
  password                = "mariadb141"     # password
  db_subnet_group_name    = aws_db_subnet_group.mariadb-subnets.name
  parameter_group_name    = aws_db_parameter_group.levelup-mariadb-parameters.name
  multi_az                = "false"            # set to true to have high availability: 2 instances synchronized with each other
  vpc_security_group_ids  = [aws_security_group.allow-mariadb.id]
  storage_type            = "gp2"
  backup_retention_period = 30                                          # how long you’re going to keep your backups
  availability_zone       = aws_subnet.levelupvpc-private-1.availability_zone # prefered AZ
  skip_final_snapshot     = true                                        # skip final snapshot when doing terraform destroy
  
  tags = {
    Name = "levelup-mariadb"
  }
}
​
output "rds" {
  value = aws_db_instance.levelup-mariadb.endpoint
}
​
7- terraform plan
8-terraform apply
9- ssh to EC2 instance and connect to MariaDB
​
$ ssh -i C:\Users\hp\.ssh\id_rsa ec2-user@ec2-52-72-232-3.compute-1.amazonaws.com
$ mysql -u root -h "mariadb.cguccmypana7.us-east-1.rds.amazonaws.com" -p
